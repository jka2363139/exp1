import React, { useState, useEffect } from 'react';
import { View, Text, Button, ScrollView, BackHandler } from 'react-native';
import { DataTable } from 'react-native-paper';

const fetchPendingList = async () => {
    try {
        const response = await fetch('http://jsonplaceholder.typicode.com/todos');
        const data = await response.json();
        return data;
    } catch (error) {
        console.error('Error fetching the pending list:', error);
        return [];
    }
};

const TaskManager = () => {
    const [pendingTasks, setPendingTasks] = useState([]);
    const [option, setOption] = useState('');
    const [results, setResults] = useState(null);
  
    useEffect(() => {
        fetchPendingList().then(data => setPendingTasks(data));
        const backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            if (option === '8') {
                BackHandler.exitApp();
                return true; 
            }
            return false; 
        });
        return () => backHandler.remove(); 
    }, [option]);

    const printPendingTasks = (tasks) => {
        return (
            <DataTable>
                <DataTable.Header>
                    <DataTable.Title>ID</DataTable.Title>
                    <DataTable.Title>Title</DataTable.Title>
                </DataTable.Header>
                {tasks.map(task => (
                    <DataTable.Row key={task.id}>
                        <DataTable.Cell>{task.id}</DataTable.Cell>
                        <DataTable.Cell>{task.title}</DataTable.Cell>
                    </DataTable.Row>
                ))}
            </DataTable>
        );
    };

    const printIDsAndUserID = (tasks) => {
        return (
            <DataTable>
                <DataTable.Header>
                    <DataTable.Title>ID</DataTable.Title>
                    <DataTable.Title>UserID</DataTable.Title>
                </DataTable.Header>
                {tasks.map(task => (
                    <DataTable.Row key={task.id}>
                        <DataTable.Cell>{task.id}</DataTable.Cell>
                        <DataTable.Cell>{task.userId}</DataTable.Cell>
                    </DataTable.Row>
                ))}
            </DataTable>
        );
    };

    const printUnresolvedTasks = () => {
        const unresolvedTasks = pendingTasks.filter(task => !task.completed);
        setResults(printPendingTasks(unresolvedTasks));
    };

    const printResolvedTasks = () => {
        const resolvedTasks = pendingTasks.filter(task => task.completed);
        setResults(printPendingTasks(resolvedTasks));
    };

    const printResolvedTasksByUserID = () => {
        const resolvedTasks = pendingTasks.filter(task => task.completed);
        setResults(printIDsAndUserID(resolvedTasks));
    };

    const printUnresolvedTasksByUserID = () => {
        const unresolvedTasks = pendingTasks.filter(task => !task.completed);
        setResults(printIDsAndUserID(unresolvedTasks));
    };

    const handleOptionSelect = async (option) => {
        switch (option) {
            case '1':
                console.log('List of all tasks (only IDs):');
                setResults(pendingTasks.map(task => <Text key={task.id}>{`ID: ${task.id}`}</Text>));
                break;
            case '2':
                console.log('List of all tasks (IDs and Titles):');
                setResults(printPendingTasks(pendingTasks));
                break;
            case '3':
                console.log('List of all unresolved tasks (ID and Title):');
                printUnresolvedTasks();
                break;
            case '4':
                console.log('List of all resolved tasks (ID and Title):');
                printResolvedTasks();
                break;
            case '5':
                console.log('List of all tasks (IDs and userID):');
                setResults(printIDsAndUserID(pendingTasks));
                break;
            case '6':
                console.log('List of all resolved tasks (ID and userID):');
                printResolvedTasksByUserID();
                break;
            case '7':
                console.log('List of all unresolved tasks (ID and userID):');
                printUnresolvedTasksByUserID();
                break;
            case '8':
                console.log('Exiting the application... Have a Good day');
                BackHandler.exitApp();
                break;
            default:
                console.log('Invalid option. Please select an option from the menu (1-8).');
                break;
        }
        setOption(option);
    };

    return (
        <ScrollView>
            <Text>Welcome to the Task Manager of NFL</Text>
            <Button title="List of all tasks (only IDs)" onPress={() => handleOptionSelect('1')} />
            <Button title="List of all tasks (IDs and Titles)" onPress={() => handleOptionSelect('2')} />
            <Button title="List of all unresolved tasks (ID and Title)" onPress={() => handleOptionSelect('3')} />
            <Button title="List of all resolved tasks (ID and Title)" onPress={() => handleOptionSelect('4')} />
            <Button title="List of all tasks (IDs and userID)" onPress={() => handleOptionSelect('5')} />
            <Button title="List of all resolved tasks (ID and userID)" onPress={() => handleOptionSelect('6')} />
            <Button title="List of all unresolved tasks (ID and userID)" onPress={() => handleOptionSelect('7')} />
            <Button title="Exit" onPress={() => handleOptionSelect('8')} />
            {results && <View>{results}</View>}
        </ScrollView>
    );
};

export default TaskManager;