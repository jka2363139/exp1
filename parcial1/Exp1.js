const fetch = require('node-fetch');
const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

async function fetchPendingList() {
    try {
        const response = await fetch('http://jsonplaceholder.typicode.com/todos');
        const data = await response.json();
        return data;
    } catch (error) {
        console.error('Error fetching the pending list:', error);
        return [];
    }
}

function printPendingTasks(pendingTasks) {
    pendingTasks.forEach(task => {
        console.log(`ID: ${task.id}, Title: ${task.title}`);
    });
}

//Esta puede aparecer como que la funcion no se lee pero sirve para facilitar el acceso a Los Uids 
//y Los titulos de cada pendiente
function printIDsAndTitles(pendingTasks) {
    pendingTasks.forEach(task => {
        console.log(`ID: ${task.id}, Title: ${task.title}`);
    });
}

function printUnresolvedTasks(pendingTasks) {
    const unresolvedTasks = pendingTasks.filter(task => !task.completed);
    console.log('List of all unresolved tasks (ID and Title):');
    printPendingTasks(unresolvedTasks);
}

function printResolvedTasks(pendingTasks) {
    const resolvedTasks = pendingTasks.filter(task => task.completed);
    console.log('List of all resolved tasks (ID and Title):');
    printPendingTasks(resolvedTasks);
}

function printIDsAndUserID(pendingTasks) {
    pendingTasks.forEach(task => {
        console.log(`ID: ${task.id}, UserID: ${task.userId}`);
    });
}

function printResolvedTasksByUserID(pendingTasks) {
    const resolvedTasks = pendingTasks.filter(task => task.completed);
    console.log('List of all resolved tasks (ID and userID):');
    printIDsAndUserID(resolvedTasks);
}

function printUnresolvedTasksByUserID(pendingTasks) {
    const unresolvedTasks = pendingTasks.filter(task => !task.completed);
    console.log('List of all unresolved tasks (ID and userID):');
    printIDsAndUserID(unresolvedTasks);
}

function showMenu() {
    console.log('Welcome to the Task Manager');
    console.log('1. List of all tasks (only IDs)');
    console.log('2. List of all tasks (IDs and Titles)');
    console.log('3. List of all unresolved tasks (ID and Title)');
    console.log('4. List of all resolved tasks (ID and Title)');
    console.log('5. List of all tasks (IDs and userID)');
    console.log('6. List of all resolved tasks (ID and userID)');
    console.log('7. List of all unresolved tasks (ID and userID)');
    console.log('8. Exit');

    rl.question('Please select an option from the menu (1-8): ', async (option) => {
        const pendingTasks = await fetchPendingList();

        switch (option) {
            case '1':
                console.log('List of all tasks (only IDs):');
                pendingTasks.forEach(task => console.log(`ID: ${task.id}`));
                break;
            case '2':
                console.log('List of all tasks (IDs and Titles):');
                printPendingTasks(pendingTasks);
                break;
            case '3':
                printUnresolvedTasks(pendingTasks);
                break;
            case '4':
                printResolvedTasks(pendingTasks);
                break;
            case '5':
                console.log('List of all tasks (IDs and userID):');
                printIDsAndUserID(pendingTasks);
                break;
            case '6':
                printResolvedTasksByUserID(pendingTasks);
                break;
            case '7':
                printUnresolvedTasksByUserID(pendingTasks);
                break;
            case '8':
                console.log('Exiting the application... Have a Good day');
                rl.close();
                break;
            default:
                console.log('Invalid option. Please select an option from the menu (1-8).');
                break;
        }

        if (option !== '8') {
            showMenu();
        } 
    });
}

showMenu();